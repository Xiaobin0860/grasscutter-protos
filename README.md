# Grasscutter Protos
This repository contains the [ProtoBuf](https://github.com/google/protobuf) `.proto` files for [Sorapointa Proto](https://github.com/Sorapointa/Sorapointa-Protos) APIs.<br/>
Then use [SoraToJava.py](/SoraToJava.py) so that it can be used for [Grasscutter](https://github.com/Grasscutters/Grasscutter).